<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(targetEntity: Places::class, mappedBy: 'place')]
    private Collection $place;

    #[ORM\OneToMany(targetEntity: Voitures::class, mappedBy: 'voiture')]
    private Collection $voiture;

    #[ORM\Column(type: Types::BIGINT)]
    private ?string $cout = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    public function __construct()
    {
        $this->place = new ArrayCollection();
        $this->voiture = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Places>
     */
    public function getPlace(): Collection
    {
        return $this->place;
    }

    public function addPlace(Places $place): static
    {
        if (!$this->place->contains($place)) {
            $this->place->add($place);
            $place->setReservation($this);
        }

        return $this;
    }

    public function removePlace(Places $place): static
    {
        if ($this->place->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getReservation() === $this) {
                $place->setReservation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Voitures>
     */
    public function getVoiture(): Collection
    {
        return $this->voiture;
    }

    // public function addVoiture(Voitures $voiture): static
    // {
    //     if (!$this->voiture->contains($voiture)) {
    //         $this->voiture->add($voiture);
    //         $voiture->setReservation($this);
    //     }

    //     return $this;
    // }

    // public function removeVoiture(Voitures $voiture): static
    // {
    //     if ($this->voiture->removeElement($voiture)) {
    //         // set the owning side to null (unless already changed)
    //         if ($voiture->getReservation() === $this) {
    //             $voiture->setReservation(null);
    //         }
    //     }

    //     return $this;
    // }

    public function getCout(): ?string
    {
        return $this->cout;
    }

    public function setCout(string $cout): static
    {
        $this->cout = $cout;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }
}
